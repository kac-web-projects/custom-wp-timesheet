<?php

function ts_create_docket_page() {
ob_start();?>

  <div id="ts-docket">
    <?php brh1color('Create/Add docket','green'); ?>

  <form action='admin.php?page=docket-create' method='post'>
  	
    <!-- Field for docket-number -->
    <div class="ts-setting">
      <label class="ts-label">Docket number :</label>
      <input class="ts-create-input" type="text" name="ts-doc-number" value="" required="true" />
      <p class="description">enter docket number - Unique</p>
    </div>

  	<!-- Field for docket-description -->
    <div class="ts-setting">
      <label class="ts-label">Description :</label>
      <br/>
      <textarea class="ts-create-input" name="ts-doc-desc" rows="4" cols="70" required="true" >some description about docket
       </textarea> 
      <p class="description">enter the docket description</p>
    </div>


  	<!-- Field for docket-budget -->
    <div class="ts-setting">
      <label class="ts-label">Max budget :</label>
      <input class="ts-create-input" type="text" name="ts-doc-budget" value="" required="true" size="4" />
      hours
      <p class="description">enter the maximum budget.</p>
    </div>

  	<!-- Field for start-date -->
    <div class="ts-setting">
      <label class="ts-label">Start date :</label>
      <input class="ts-create-input" type="date" name="ts-doc-sdate" value="" required="true" />
      <p class="description">start or opening date for docket.</p>
    </div>

	<!-- Field for end-date -->
    <div class="ts-setting">
      <label class="ts-label">End date :</label>
      <input class="ts-create-input" type="date" name="ts-doc-edate" value="" required="true" />
      <p class="description">end or closing date for docket.</p>
    </div>

  	<!-- Field for email-on-max -->
    <div class="ts-setting">
      <label class="ts-label">Email : </label>
      <input class="ts-create-input" type="checkbox" name="ts-email-onmax" value="yes" />
      <p class="description">notify when maximum budget is reached</p>
    </div>

  	<!-- Field for notify-email -->
    <div class="ts-setting">
      <label class="ts-label">Notify email :</label>
      <input class="ts-create-input" type="text" name="ts-notify-email" value="" size="40"/>
      <p class="description">specify email to be notified</p>
    </div>

    <!-- Field for status -->
    <div class="ts-setting">
      <label class="ts-label">Status :</label>
      <select class="ts-create-select"  name="ts-status" required="true">
        <option value="active">Active</option>
        <option value="inactive">Inactive</option>
        <option value="hold">Hold</option>
      </select>
      <p class="description">docket current status</p>
     </div>

    <!-- submit and reset button -->
    <div class="ts-create">
      <br/><br/>
      <input type="submit" value="Create" class="button-primary" />
      <input type="button" value="Reset" onClick="history.go(0)" class="button-primary" />
      <input type="button" value="View dockets" class="button-primary" onClick='location.href="?page=view-dockets"' />
      <input type="button" value="By status" class="button-primary" onClick='location.href="?page=view-dockets-bystatus"' />


    </div>

  </form>
</div>

<?php
echo ob_get_clean();
}