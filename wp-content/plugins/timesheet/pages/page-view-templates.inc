<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';

function ts_view_templates_page() {
ob_start();?>

	<div id="ts-view-templates">
    	<?php
      brh1color('View templates','green');
      ?>

    <table style="width:300px">

      <thead>
        <?php
          
            th_width('#','20px');
            th_width('Name','100px');
            th_width('Subject','200px');
            
            th_width('EDIT','40px');

          ?>
    
      </thead>

      <tbody>
      <?php
      $records = get_templates();
      $srno = 0;
      $empty = TRUE;
      foreach ($records as $record) {
        $empty = FALSE;

        print("<tr>");
        td_width(++$srno,'20px');
        td_width($record->name,'100px');
        td_width($record->subject,'200px');

        td_width("<a href=\"?page=edit-template&tid=$record->tid\" target=\"_blank\"> Edit</a>",'40px');

        print("</tr><tr><td colspan=12><hr/></tr>");
              
      }

      if($empty) {
          print("<tr rowspan=4><td colspan=10>");
          brh4color("<br/>No templates found!<br/><br/>",'darkkhaki');
          print("</td></tr>");
      }
      ?>
   
      
      </tbody>

    </table>
  	
</div>

<?php
echo ob_get_clean();
}