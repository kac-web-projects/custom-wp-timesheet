<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';
include_once TS_PLUGIN_DIR .'/queries.inc';
include_once TS_PLUGIN_DIR.'/email/mail.inc';


function ts_update_template_handler() {

	$tid = $_POST['ts-tid'];
	$name = $_POST['ts-name'];
	$subject = $_POST['ts-subject'];
	$body = $_POST['ts-body'];

	//If extra space is been added at start or end
	$subject = trim($subject);
	$body = trim($body);
	$body = "<pre> $body </pre>";


	global $wpdb;
	$table_name = $wpdb->prefix . "timesheet_templates"; 

	$result = $wpdb->update( 
		$table_name,
			array(
				'name' => $name,
				'subject' => $subject,
				'body' => $body,
				),
			array('tid' => $tid),
			
			array('%s' , '%s', '%s'),

			array('%d')
		);

	if($result) {
		
		brh1color('Template updated successfully!','green');
		brh2color('Below are updated template details','darkkhaki');

		brstrong_normal('Name', $name);
		brstrong_normal('Subject', $subject);
		brstrong_normal('Body', $body);
		
		}
	else {
		brh1color('Something went wrong!','red');
		brh4color("Sorry for inconvience, please <a href=#> Try again</a>",'darkkhaki');
	}

	ob_start();?>
		
		<div class="ts-update">
      	<br/><br/>
      	<input type="button" value="Back to template" onClick="history.go(-1)" class="button-primary" />
      	<input type="button" value="View templates" onClick='location.href="?page=view-templates"' class="button-primary" />
      	
    	</div>
    	
    	<?php
		echo ob_get_clean();
}