/*
For page-view-dockets-bystatus.inc page
For dockets management => Active, Inactive, Hold
*/

function docket_display(show,hide_F,hide_S) {
	document.getElementById(show).style.display = 'block';

	document.getElementById(hide_F).style.display = 'none';
	document.getElementById(hide_S).style.display = 'none';
}

