<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';
include_once TS_PLUGIN_DIR .'/queries.inc';
include_once TS_PLUGIN_DIR.'/email/mail.inc';


function ts_update_docket_handler() {

	$did = $_POST['ts-did'];
	$doc_num = $_POST['ts-doc-num'];
	$doc_desc = $_POST['ts-doc-desc'];
	$doc_budget = $_POST['ts-doc-budget'];
	$doc_sdate = $_POST['ts-doc-sdate'];
	$doc_edate = $_POST['ts-doc-edate'];
	$email_onmax = $_POST['ts-email-onmax'];
	$notify_email = $_POST['ts-notify-email'];
	$added_by = $_POST['ts-added-by'];
	$status = $_POST['ts-status'];
	
	if(empty($email_onmax))
		$email_onmax = 'no';
	
	//If extra space is been added at start or end
	$doc_desc = trim($doc_desc);
	$doc_budget = trim($doc_budget);
	$notify_email = trim($notify_email);

	global $wpdb;
	$table_name = $wpdb->prefix . "timesheet_dockets"; 

	$result = $wpdb->update( 
		$table_name,
			array(
				'docket_desc' => $doc_desc,
				'docket_budget' => $doc_budget,
				'start_date' => $doc_sdate,
				'end_date' => $doc_edate,
				'email_onmax' => $email_onmax,
				'notify_email' => $notify_email,
				'docket_status' => $status,
				),
			array('did' => $did),
			
			array('%s' , '%s', '%s', '%s', '%s', '%s' , '%s'),

			array('%d')
		);

	if($result) {
		
		brh1color('Docket updated successfully!','green');
		brh2color('Below are updated docket details','darkkhaki');

		brstrong_normal('Docket id', $did);
		brstrong_normal('Docket number', $doc_num);
		brstrong_normal('Description', $doc_desc);
		brstrong_normal('Max budget' , $doc_budget);
		brstrong_normal('Start date', $doc_sdate);
		brstrong_normal('End date', $doc_edate);
		brstrong_normal('Notify', $email_onmax);
		brstrong_normal('Notify email', $notify_email);
		brstrong_normal('Added by', $added_by);
		brstrong_normal('Status', $status);

		// check_budget($doc_num);
		}
	else {
		brh1color('Something went wrong!','red');
		brh4color("Sorry for inconvience, please <a href=#> Try again</a>",'darkkhaki');
	}

	ob_start();?>
		
		<div class="ts-update">
      	<br/><br/>
      	<input type="button" value="Back to Docket" onClick="history.go(-1)" class="button-primary" />
      	<input type="button" value="View dockets" onClick='location.href="?page=view-dockets"' class="button-primary" />
      	<input type="button" value="By status" onClick='location.href="?page=view-dockets-bystatus"' class="button-primary" />
      	
    	</div>
    	
    	<?php
		echo ob_get_clean();


}