<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';
include_once TS_PLUGIN_DIR .'/queries.inc';

function ts_delete_timesheet_handler() {

	$rid = $_POST['ts-rid'];
	global $wpdb;
	$table_name = $wpdb->prefix . "timesheet_master"; 

	$result = $wpdb->delete($table_name, array('rid' => $rid));

	if($result) {
		brh1color('Timesheet deleted successfully!','green');
	}
	else {
		brh1color('Something went wrong!','red');
		brh4color("Sorry for inconvience, please <a href=#> Try again</a>",'darkkhaki');
	}

	ob_start();?>
		
		<div class="ts-update">
      	<br/><br/>
      	<input type="button" value="View timesheets" onClick='location.href="?page=view-timesheets"' class="button-primary" />
    	</div>
    	
    	<?php
		echo ob_get_clean();


}