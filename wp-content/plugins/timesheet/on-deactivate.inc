<?php
/*
Custom function to invoke when plugin is disable(deactivated)
*/
function timesheet_deactivation(){
	error_log("Notify : Timesheet plugin de-activated");
	drop_table("timesheet_dockets");
   	drop_table("timesheet_master");
   	drop_table("timesheet_templates");
}

/*
Removing Installing timesheet_dockets table
*/
function drop_table($table_name) {
	global $wpdb;
	$table_name = $wpdb->prefix . $table_name;
	$sql = "DROP TABLE IF EXISTS $table_name;";
	$e = $wpdb->query($sql);
	// die(var_dump($e));

	error_log("Notify : $table_name table is been removed");
}

//Revoke Editor role permission to manege_options
function remove_plugin_caps() {
    // gets the author role
    $role = get_role( 'editor' );

    // This only works, because it accesses the class instance.
    // would allow the author to edit others' posts for current theme only
    $role->remove_cap( 'manage_options' ); 
}
