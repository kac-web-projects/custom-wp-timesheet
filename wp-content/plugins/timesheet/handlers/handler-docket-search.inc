<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';
include_once TS_PLUGIN_DIR .'/queries.inc';

function ts_search_docket_handler() {

	$doc_num = $_POST['ts-doc-num'];
	$result = get_docket_info_dnum($doc_num);
	
	if($result) {
		
		brh1color('Docket loaded successfully!','green');
		brh2color('Below are docket details','darkkhaki');

		$did = $result[0]->did;
		brstrong_normal('Docket number', $result[0]->docket_num);
		brstrong_normal('Description', $result[0]->docket_desc);
		brstrong_normal('Max budget' , $result[0]->docket_budget . " hrs");
		brstrong_normal('Start date', $result[0]->start_date);
		brstrong_normal('End date', $result[0]->end_date);
		brstrong_normal('Notify', $result[0]->email_onmax);
		brstrong_normal('Notify email', $result[0]->notify_email);
		brstrong_normal('Status', $result[0]->docket_status);

		brh4color("You can edit it <a href=\"?page=edit-docket&did=$did\" target=\"_blank\" >here</a>",'green');

		}
	else {
		brh1color('Something went wrong!','red');
		brh4color("Sorry for inconvience, please <a href=#> Try again</a>",'darkkhaki');
	}

	ob_start();?>
		
		<div class="ts-update">
      	<br/><br/>
      	<input type="button" value="Goto search (back)" onClick="history.go(-1)" class="button-primary" />
      	<input type="button" value="View dockets" onClick='location.href="?page=view-dockets"' class="button-primary" />
      	<input type="button" value="By status" onClick='location.href="?page=view-dockets-bystatus"' class="button-primary" />
      	
    	</div>
    	
    	<?php
		echo ob_get_clean();


}