<?php

function ts_templates($templates){
  // view docket template
  $templates['view_dockets'] = array(
    'files' => 'pages/page-view-dockets.inc',
    'default_path' => TS_PLUGIN_DIR,
  );

  // create docket template
  $templates['create_docket'] = array(
    'files' => 'pages/page-create-docket.inc',
    'default_path' => TS_PLUGIN_DIR,
  );

  // useful for plugin admin pages
  $templates['admin_wrapper'] = array(
    'files' => array(
        'admin/wrapper-admin.inc',
        'wrapper-admin.inc',
    ),
    'default_path' => TS_PLUGIN_DIR,
    'arguments' => array(
        'title' => 'Admin Page',
        'content' => 'content goes here.',
    ),
  );
  
  return $templates;
}

// add_filter('tw_templates', 'ts_templates');
