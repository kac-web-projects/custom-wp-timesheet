<?php
function get_dockets() {
  global $wpdb;
  $table_name = $wpdb->prefix . "timesheet_dockets"; 
  $result = $wpdb->get_results("SELECT * FROM $table_name", OBJECT);
  return $result;
}

function get_docket_info_did($did) {
  global $wpdb;
  $table_name = $wpdb->prefix . "timesheet_dockets"; 
  $result = $wpdb->get_results("SELECT * FROM $table_name WHERE did = $did ", OBJECT);
  return $result;
}

function get_docket_info_dnum($doc_num) {
  global $wpdb;
  $table_name = $wpdb->prefix . "timesheet_dockets"; 
  $result = $wpdb->get_results("SELECT * FROM $table_name WHERE docket_num = '$doc_num' ", OBJECT);
  return $result;
}

function get_dockets_bystatus($status) {
  global $wpdb;
  $table_name = $wpdb->prefix . "timesheet_dockets"; 
  $result = $wpdb->get_results("SELECT * FROM $table_name WHERE docket_status = '$status' ", OBJECT);
  return $result;

}

//Timesheet Master Table
function get_timesheets() {
  global $wpdb;
  $table_name = $wpdb->prefix . "timesheet_master"; 
  $result = $wpdb->get_results("SELECT * FROM $table_name", OBJECT);
  return $result;
}

function check_duplicate($rdate,$task_num,$doc_num) {
  global $wpdb;
  $table_name = $wpdb->prefix . "timesheet_master"; 
  $result = $wpdb->get_results("SELECT * FROM $table_name WHERE rdate = '$rdate' AND task_num = '$task_num' AND docket_num = '$doc_num' ", OBJECT);
  return $result;
}

function get_timesheet_info_rid($rid) {
  global $wpdb;
  $table_name = $wpdb->prefix . "timesheet_master"; 
  $result = $wpdb->get_results("SELECT * FROM $table_name WHERE rid = $rid", OBJECT);
  return $result;
}

function get_timesheet_info_dnum($doc_num) {
  global $wpdb;
  $table_name = $wpdb->prefix . "timesheet_master"; 
  $result = $wpdb->get_results("SELECT * FROM $table_name WHERE docket_num = '$doc_num' ", OBJECT);
  return $result;
}

//Count number of hours recorded/tracked for specific docket number
function count_timesheet_hours($doc_num){
  global $wpdb;
  $table_name = $wpdb->prefix . "timesheet_master"; 
  $result = $wpdb->get_var("SELECT sum(time_spent) FROM $table_name WHERE docket_num = '$doc_num' ");
  return $result;
}

//Templates Table
function get_templates() {
  global $wpdb;
  $table_name = $wpdb->prefix . "timesheet_templates"; 
  $result = $wpdb->get_results("SELECT * FROM $table_name", OBJECT);
  return $result;
}

//Get template info
function get_template_info_tid($tid) {
  global $wpdb;
  $table_name = $wpdb->prefix . "timesheet_templates"; 
  $result = $wpdb->get_results("SELECT * FROM $table_name WHERE tid = '$tid' ", OBJECT);
  return $result;
}

//Show columns
function get_cols_table($table_name) {
  global $wpdb;
  $table_name = $wpdb->prefix . $table_name; 
  $result = $wpdb->get_results("SHOW columns FROM $table_name", OBJECT);
  return $result;
}