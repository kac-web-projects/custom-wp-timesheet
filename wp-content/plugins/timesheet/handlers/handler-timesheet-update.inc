<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';
include_once TS_PLUGIN_DIR .'/queries.inc';
include_once TS_PLUGIN_DIR.'/email/mail.inc';

function ts_update_timesheet_handler() {

	$rid = $_POST['ts-rid'];
	$rdate = $_POST['ts-rdate'];
	$task_num = $_POST['ts-task-num'];
	$doc_num = $_POST['ts-doc-num'];
	$time_spent = $_POST['ts-time-spent'];
	$task_desc = $_POST['ts-task-desc'];

	//If extra space is been added at start or end
	$task_num = trim($task_num);
	$doc_num = trim($doc_num);
	$time_spent = trim($time_spent);
	$task_desc = trim($task_desc);

	global $wpdb;
	$table_name = $wpdb->prefix . "timesheet_master"; 

	$result = $wpdb->update( 
		$table_name,
			array(
				'rdate' => $rdate,
				'task_num' => $task_num,
				'docket_num' => $doc_num,
				'time_spent' => $time_spent,
				'task_desc' => $task_desc,
				),
			array('rid' => $rid),
			
			array('%s' , '%s', '%s', '%s', '%s'),

			array('%d')
		);

	if($result) {
		
		brh1color('Timesheet entry updated successfully!','green');
		brh2color('Below are updated details','darkkhaki');

		brstrong_normal('Entry date', $rdate);
		brstrong_normal('Task number', $task_num);
		brstrong_normal('Docket number', $doc_num);
		brstrong_normal('Time spent', $time_spent . " hrs" );
		brstrong_normal('Task desc', $task_desc);
		
		check_budget($doc_num);

		}
	else {
		brh1color('Something went wrong!','red');
		brh4color("Sorry for inconvience, please <a href=#> Try again</a>",'darkkhaki');
	}

	ob_start();?>
		
		<div class="ts-update">
      	<br/><br/>
      	<input type="button" value="Back to Timesheet" onClick="history.go(-1)" class="button-primary" />
      	<input type="button" value="View timesheet" onClick='location.href="?page=view-timesheets"' class="button-primary" />
      	
    	</div>
    	
    	<?php
		echo ob_get_clean();


}