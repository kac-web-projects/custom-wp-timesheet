<?php
/*
Custom function to invoke when plugin is enabled (activated)
*/
function timesheet_activation(){
	global $wpdb;

	/*
	 * We'll set the default character set and collation for this table.
	 * If we don't do this, some characters could end up being converted 
	 * to just ?'s when saved in our table.
	 */
	$charset_collate = '';

	if ( ! empty( $wpdb->charset ) ) {
	  $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
	}

	if ( ! empty( $wpdb->collate ) ) {
	  $charset_collate .= " COLLATE {$wpdb->collate}";
	}

   	$table_name = $wpdb->prefix . "timesheet_dockets"; 
	create_docket_table($table_name,$charset_collate); //custom function to install docket table
   	
   	$table_name = $wpdb->prefix . "timesheet_master"; 
	create_timesheet_table($table_name,$charset_collate); //custom function to install timesheet table

   	$table_name = $wpdb->prefix . "timesheet_templates"; 
   	//custom function to install timesheet templates table
	create_templates_table($table_name,$charset_collate);

	//custom function to fill data into custom tabel
	add_data_templates_table($table_name);
	error_log("Notify : Timesheet plugin activated");
}

/*
Creating timesheet_dockets table on Plugin Activated.
*/
function create_docket_table($table_name, $charset_collate) {
	
	$sql = "CREATE TABLE $table_name (
	  did mediumint(9) NOT NULL AUTO_INCREMENT,
	  docket_num VARCHAR(30) NOT NULL,
	  docket_desc VARCHAR(255) DEFAULT '' NOT NULL,
	  docket_budget tinytext NOT NULL,
	  start_date date DEFAULT '0000-00-00' NOT NULL,
	  end_date date DEFAULT '0000-00-00' NOT NULL,
	  email_onmax VARCHAR(3) DEFAULT 'yes' NOT NULL,
	  notify_email VARCHAR(50) default 'user@example.com' NOT NULL,
	  docket_status VARCHAR(10) default 'active' NOT NULL,

	  added_by VARCHAR(20) DEFAULT '' NOT NULL,

	  PRIMARY KEY (did),
	  UNIQUE KEY (docket_num)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta($sql);
	error_log("Notify : timesheet_dockets table created");
}

/*
Creating timesheet_master table on Plugin Activated.
*/
function create_timesheet_table($table_name, $charset_collate) {
	 
	$sql = "CREATE TABLE $table_name (
	  rid mediumint(9) NOT NULL AUTO_INCREMENT,
	  rdate date DEFAULT '0000-00-00' NOT NULL,
	  task_num tinytext NOT NULL,
	  docket_num VARCHAR(30) NOT NULL,
	  time_spent text NOT NULL,
	  task_desc VARCHAR(255) DEFAULT '' NOT NULL,
	  status text NOT NULL,
	  
	  added_by VARCHAR(20) DEFAULT '' NOT NULL,
	  
	  PRIMARY KEY (rid)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta($sql);
	error_log("Notify : timesheet_master table created");
}

/*
Creating timesheet_templates table on Plugin Activated.
*/
function create_templates_table($table_name, $charset_collate) {
	 
	$sql = "CREATE TABLE $table_name (
	  tid mediumint(9) NOT NULL AUTO_INCREMENT,
	  name VARCHAR(10) NOT NULL,
	  subject VARCHAR(30) NOT NULL,
	  body VARCHAR(1000) DEFAULT '' NOT NULL,
	  example VARCHAR(1000) DEFAULT '' NOT NULL,
	  
	  PRIMARY KEY (tid)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta($sql);
	error_log("Notify : timesheet_templates table created");
}

/*
Add data into templates-table
*/
function add_data_templates_table($table_name) {
	$body ="<pre> Hi {added_by}, 

Your are been informed that docket {docket_num}  has reached the maximum budget of {docket_budget} hours.

Currently docket status is {docket_status}, you can change it to inactive or hold to prevent adding more time on this docket.

Docket Desc : {docket_desc}
Start : {start_date}
End : {end_date}
		
To stop receiving email uncheck notify option in docket edit.

You are receiving this email since your email is been added to notify you.

Thank you for using Independent Timesheet Management Tool </pre>";

$example = "Hi {added_by}, 

Your are been informed that docket {docket_num}  has reached the maximum budget of {docket_budget} hours.

Currently docket status is {docket_status}, you can change it to inactive or hold to prevent adding more time on this docket.

Docket Desc : {docket_desc}
Start : {start_date}
End : {end_date}
		
To stop receiving email uncheck notify option in docket edit.

You are receiving this email since your email is been added to notify you.

Thank you for using Independent Timesheet Management Tool";
	
	$subject = "Max-budget reached";
	global $wpdb;
	$result = $wpdb->insert( 
		$table_name, 
		array( 
			'name' => 'manager', 
			'subject' => $subject,
			'body' => $body, 
			'example' => $example, 
		) 
	);

	if($result)
		error_log("Notify : Default data field into timesheet_templates table");
}
//Granting Editor role permission to manege_options
function add_plugin_caps() {
    // gets the author role
    $role = get_role( 'editor' );

    // This only works, because it accesses the class instance.
    // would allow the author to edit others' posts for current theme only
    $role->add_cap( 'manage_options' ); 
}