<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';

function ts_edit_template_page() {
	$href = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

	$url = parse_url($href);
	
	$query = array();
	parse_str($url['query'], $query);

	$tid = $query['tid'];
	$load_info = get_template_info_tid($tid);

ob_start();?>
	<div id="ts-edit-template" style="width:500px;float: left">
    	<?php brh1color('Edit template','green'); ?>

  <form action='admin.php?page=template-update' method='post'>

	<!-- Field for docket-id -->
      <input class="ts-edit-input" type="hidden" name="ts-tid" value="<?php print $tid; ?>" readonly="readonly" />

  	<!-- Field for Name -->
    <div class="ts-setting">
      <label class="ts-label">Name :</label>
      <input class="ts-edit-input" type="text" name="ts-name" value="<?php print $load_info[0]->name ?>" readonly="readonly" />
      <p class="description">template name- Readonly</p>
    </div>

    <!-- Field for Subject -->
    <div class="ts-setting">
      <label class="ts-label">Subject :</label>
      <input class="ts-edit-input" type="text" name="ts-subject" value="<?php print $load_info[0]->subject ?>" size="48"/>
      <p class="description">enter subject for mail</p>
    </div>

  	<!-- Field for Body -->
    <div class="ts-setting">
      <label class="ts-label">Body :</label>
      <br/>
    <!-- $body = str_replace($field, $replace,$body);   -->
      <textarea class="ts-edit-input" name="ts-body" rows="20" cols="55" required="true" ><?php
        $body = strip_tags($load_info[0]->body);
        $body = trim($body);
        print($body);
        ?>
      </textarea>
      <p class="description">enter the body for mail</p>
    </div>

    <div class="ts-setting">
      <label class="ts-label">Tokens :</label>
      <p class="description">use token to replace actual value</p>
      
      <?php $records = get_cols_table("timesheet_dockets");
      foreach ($records as $record) {
        print("<br/>");
        print("{" . $record->Field . "}");
      }
      
    ?>

    </div>

    <!-- update and reset button -->
    <div class="ts-create">
      <br/><br/>
      <input type="submit" value="Update" class="button-primary" />
      <input type="button" value="Reset" onClick="history.go(0)" class="button-primary" />
    </div>

  </form>
</div>

<div id= "ts-edit-template-right" style="float:left; padding-top : 180px">

    <!-- Field for Example -->
    <div class="ts-setting">
      <label class="ts-label">Example :</label>
      <br/>
      <textarea class="ts-edit-input" name="ts-example" rows="20" cols="55" required="true"  disabled="disabled"><?php print $load_info[0]->example ;?>
      </textarea>
      <p class="description">Usage sample</p>
    </div>

</div>


<?php
echo ob_get_clean();
}