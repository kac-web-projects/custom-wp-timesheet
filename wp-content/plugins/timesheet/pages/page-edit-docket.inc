<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';

function ts_edit_docket_page() {
	$href = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

	$url = parse_url($href);
	
	$query = array();
	parse_str($url['query'], $query);

	$did = $query['did'];
	$load_info = get_docket_info_did($did);

ob_start();?>

	<div id="ts-edit-docket">
    	<?php brh1color('Edit docket','green'); ?>

  <form action='admin.php?page=docket-update' method='post'>

	<!-- Field for docket-id -->
      <input class="ts-edit-input" type="hidden" name="ts-did" value="<?php print $did; ?>" readonly="readonly" />

  	<!-- Field for docket-number -->
    <div class="ts-setting">
      <label class="ts-label">Docket number :</label>
      <input class="ts-edit-input" type="text" name="ts-doc-num" value="<?php print $load_info[0]->docket_num ?>" readonly="readonly" />
      <p class="description">docket number - Readonly</p>
    </div>

  	<!-- Field for docket-description -->
    <div class="ts-setting">
      <label class="ts-label">Description :</label>
      <br/>
      <textarea class="ts-edit-input" name="ts-doc-desc" rows="4" cols="70" required="true" ><?php print $load_info[0]->docket_desc ;?>
      </textarea>
      <p class="description">enter the docket description</p>
    </div>

  	<!-- Field for docket-budget -->
    <div class="ts-setting">
      <label class="ts-label">Max budget :</label>
      <input class="ts-edit-input" type="text" name="ts-doc-budget" value="<?php print $load_info[0]->docket_budget ?>" size="4"/>
      <p class="description">enter the maximum budget</p>
    </div>

  	<!-- Field for start-date -->
    <div class="ts-setting">
      <label class="ts-label">Start date :</label>
      <input class="ts-edit-input" type="date" name="ts-doc-sdate" value="<?php print $load_info[0]->start_date ?>" readonly="readonly" />
      <p class="description">start or opening date for docket - Readonly</p>
    </div>

   <!-- Field for end-date -->
    <div class="ts-setting">
      <label class="ts-label">End date :</label>
      <input class="ts-edit-input" type="date" name="ts-doc-edate" value="<?php print $load_info[0]->end_date ?>" />
      <p class="description">end or closing date for docket</p>
    </div>

  	<!-- Field for email-on-max -->
    <div class="ts-setting">
      <label class="ts-label">Email : </label>
      
      <?php if($load_info[0]->email_onmax == 'yes') {?>
      <input class="ts-edit-input" type="checkbox" name="ts-email-onmax" value="yes" checked="" />
      <?php } else { ?>
      <input class="ts-edit-input" type="checkbox" name="ts-email-onmax" value="yes" />
      <?php } ?>
      
      <p class="description">notify when maximum budget is reached</p>
    </div>

  	<!-- Field for notify-email -->
    <div class="ts-setting">
      <label class="ts-label">Notify email :</label>
      <input class="ts-edit-input" type="text" name="ts-notify-email" value="<?php print $load_info[0]->notify_email ?>" size="40" />
      <p class="description">specify email to be notified</p>
    </div>

	<!-- Field for added-by -->
    <div class="ts-setting">
      <label class="ts-label">Added by :</label>
      <input class="ts-edit-input" type="text" name="ts-added-by" value="<?php print $load_info[0]->added_by?>" readonly="readonly" />
      <p class="description">addedby</p>
    </div>

    <!-- Field for status -->
    <div class="ts-setting">
      <label class="ts-label">Status :</label>
        <select class="ts-edit-select"  name="ts-status" required="true">
        
        <?php if($load_info[0]->docket_status == 'active') { ?>
        <option value="active" selected="selected" >Active</option>
        <option value="inactive">Inactive</option>
        <option value="hold">Hold</option>
        <?php } else if ($load_info[0]->docket_status == 'inactive') { ?>
        <option value="active" >Active</option>
        <option value="inactive" selected="selected" >Inactive</option>
        <option value="hold">Hold</option>
        <?php } else { ?>
        <option value="active" >Active</option>
        <option value="inactive">Inactive</option>
		<option value="hold" selected="selected">Hold</option>
        <?php } ?>

      </select>
      <p class="description">docket current status</p>
     </div>

    <!-- update and reset button -->
    <div class="ts-create">
      <br/><br/>
      <input type="submit" value="Update" class="button-primary" />
      <input type="button" value="Reset" onClick="history.go(0)" class="button-primary" />
    </div>

  </form>
</div>

<?php
echo ob_get_clean();
}