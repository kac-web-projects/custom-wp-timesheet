<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';

function ts_view_dockets_bystatus_page() {
ob_start();?>
  <head>
      <script src="<?php print(PLUGIN_URL . '/js/pages.js'); ?>"></script>
  </head>

	<div id="ts-view-dockets">
    	<?php
      brh1color('Dockets by status','green');
      ?>
    
      <input type="button" value="Active dockets" class="button-primary" onclick="javascript:docket_display('active','inactive','hold');"/>
    
      <input type="button" value="Inactive dockets" class="button-primary" onclick="javascript:docket_display('inactive','active','hold');"/>
    
      <input type="button" value="Hold dockets" class="button-primary" onclick="javascript:docket_display('hold','active','inactive');"/>
      <br/><br/><br/>

      <?php
      $status = array('active','inactive','hold');

    
      for($i = 0; $i < 3; $i++) {
        if($i == 0)
        print("<div id=\"$status[$i]\" >");//Start div
        else
        print("<div id=\"$status[$i]\" style=\"display:none\">");//Start div

        ob_start();?>
        <table style="width:300px">
          <thead>
          <tr>
    
          <?php
          
            th_width('#','20px');
            th_width('Docket','100px');
            th_width('Description','200px');
            th_width('Budget (hrs)','40px');
            th_width('Start-date','80px');
            th_width('End-date','80px');
            th_width('Notify','40px');
            th_width('Email','150px');
            th_width('Status','50px');
            th_width('Added-by','60px');
            
            th_width('EDIT','40px');
            th_width('DEL','40px');

          ?>
          </tr>
          </thead>

          <tbody>
        <?php
        echo ob_get_clean();
        
            $records = get_dockets_bystatus($status[$i]);
            $srno = 0;
            $empty = TRUE;
              foreach ($records as $record) {
                $empty = FALSE;

                print("<tr>");
                td_width(++$srno,'20px');
                td_width($record->docket_num,'100px');
                td_width($record->docket_desc,'200px');
                td_width($record->docket_budget,'40px');
                td_width($record->start_date,'80px');
                td_width($record->end_date,'80px');
                td_width($record->email_onmax,'40px');
                td_width($record->notify_email,'150px');
                td_width($record->docket_status,'50px');
                td_width($record->added_by,'50px');
                td_width("<a href=\"?page=edit-docket&did=$record->did\" target=\"_blank\"> Edit</a>",'40px');
                td_width("<a href=\"?page=delete-docket&did=$record->did\" target=\"_blank\"> Delete</a></td>",'40px');

                print("</tr><tr><td colspan=12><hr/></tr>");
              }

              if($empty) {
                  print("<tr rowspan=4><td colspan=10>");
                  brh4color("<br/>No \"$status[$i]\" docket entry found!<br/><br/>",'darkkhaki');
                  print("</td></tr>");
              }
            
              ob_start();?>
                <tr>
                <td colspan="20"><hr/></td>
                </tr>

                <tr rowspan = "4">
                <td colspan = "10">
                <input type="button" value="Add docket" onClick='location.href="?page=create-docket"' class="button-primary" />
                </td>
                </tr>
                 </tbody>
              </table>

              <?php
              echo ob_get_clean();
        
        print("</div>");//Close div
        }
    ?>

</div>

<?php
echo ob_get_clean();
}