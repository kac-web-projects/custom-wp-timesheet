<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';
include_once TS_PLUGIN_DIR .'/queries.inc';

function ts_create_docket_handler() {

	$doc_num = $_POST['ts-doc-number'];
	$doc_desc = $_POST['ts-doc-desc'];
	$doc_budget = $_POST['ts-doc-budget'];
	$doc_sdate = $_POST['ts-doc-sdate'];
	$doc_edate = $_POST['ts-doc-edate'];
	$email_onmax = $_POST['ts-email-onmax'];
	$notify_email = $_POST['ts-notify-email'];
	
	//If checkbox is unchecked
	if(empty($email_onmax))
		$email_onmax = 'no';

	//If extra space is been added at start or end
	$doc_desc = trim($doc_desc);
	$doc_budget = trim($doc_budget);
	$notify_email = trim($notify_email);


	$user = wp_get_current_user();
	$added_by = $user->user_login;

	global $wpdb;
	$table_name = $wpdb->prefix . "timesheet_dockets"; 
	$docket_exists = get_docket_info_dnum($doc_num);
	if(empty($docket_exists)) {
		$result = $wpdb->query(
			$wpdb->prepare( 
			"
			INSERT INTO $table_name
			(docket_num, docket_desc, docket_budget, start_date, end_date, email_onmax, notify_email, added_by)
			VALUES ( %s, %s, %s, %s, %s, %s, %s, %s)
			", 
			array(
				$doc_num,
				$doc_desc,
				$doc_budget,
				$doc_sdate,
				$doc_edate,
				$email_onmax,
				$notify_email,
				$added_by
			) 
		));

		$wpdb->show_errors();

		if($result) {
			
			brh1color('New docket created successfully!','green');
			brh2color('Below are docket details : ','darkkhaki');

			brstrong_normal('Docket number', $doc_num);
			brstrong_normal('Description', $doc_desc);
			brstrong_normal('Max budget' , $doc_budget);
			brstrong_normal('Start date', $doc_sdate);
			brstrong_normal('End date', $doc_edate);
			brstrong_normal('Notify', $email_onmax);
			brstrong_normal('Notify email', $notify_email);
			brstrong_normal('Added by', $added_by);
		}
		else {
			brh1color('Something went wrong!','red');
			brh4color("Sorry for inconvience, please <a href=#> Try again</a>",'green');
		}
	}
	else {

		brh3color("Docket $doc_num already exists, must be unique always!",'red');
		brh1color('Below are docket details :','darkkhaki');
		
		brstrong_normal('Docket number',$docket_exists[0]->docket_num);
		$did = $docket_exists[0]->did;
		brstrong_normal('Description', $docket_exists[0]->docket_desc);
		brstrong_normal('Max budget', $docket_exists[0]->docket_budget);
		brstrong_normal('Start date', $docket_exists[0]->start_date);
		brstrong_normal('End date', $docket_exists[0]->end_date);
		brstrong_normal('Notify', $docket_exists[0]->email_onmax);
		brstrong_normal('Notify email', $docket_exists[0]->notify_email);
		brstrong_normal('Added by', $docket_exists[0]->added_by);

		brh4color("You can edit it <a href=\"?page=edit-docket&did=$did\" target=\"_blank\" >here</a>",'green');
	}

	ob_start();?>
		
		<div class="ts-update">
      	<br/><br/>
      	<input type="button" value="Go back" onClick="history.go(-1)" class="button-primary" />

      	<input type="button" value="View dockets" onClick='location.href="?page=view-dockets"' class="button-primary" />
      	
	    <input type="button" value="By status" class="button-primary" onClick='location.href="?page=view-dockets-bystatus"' />

      	<input type="button" value="Create new docket" onClick='location.href="?page=create-docket"' class="button-primary" />
      	
    	</div>
    	
    	<?php
		echo ob_get_clean();
}