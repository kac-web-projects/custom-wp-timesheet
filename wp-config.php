<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'custom-wp-timesheet');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'O>{]|?r^)46.z.3t?Eu)IR6c!9EUC62p<w R]y6yr0/&Ij+62|+Ki,XWJvQrES5I');
define('SECURE_AUTH_KEY',  '~Qf+kEBf<m-9z{6|& 9g.x8%}iW|x )U$hHg{XFjTfti?=X1c6_m:-c`T|~>tamW');
define('LOGGED_IN_KEY',    'X $MaXw1=:lwb8PYqjWDK@u-q#W0IrrDVc ;6brVJ^*u->#~sx:V?%Lf5A;zB:Ej');
define('NONCE_KEY',        '-:LZlmzC</os8+105|{|)^vj_{+vz|t-d-8Vk-<Y#$[J@M@A6U+,?RrcBP={Qx@Y');
define('AUTH_SALT',        '[dGlNfqG/tmCd*Ef w{wE9P<a`ANXoU(U}<A8#:n*|S_Q^]-r3^Dmmn 4FMw Q?@');
define('SECURE_AUTH_SALT', 'Cm8*y$[BW2g#Y+zuN=-F&Z2b=DIl?q4|EmSNwGe|K9Jdg0VI|cBH&?DD%($`(5a6');
define('LOGGED_IN_SALT',   '`%0su;Ovb.ZlqO%|/IT}$x@=0Wf7@O!,+bX@p5_?-RF683VjsLFL-C$k$dRg~Ks5');
define('NONCE_SALT',       '`I+$!jJTHHCuy;d{Un/-)I+?/!lRRIl!vC|H2|5D~s|]mmCH%dhGj&FLkRDSDBnx');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
