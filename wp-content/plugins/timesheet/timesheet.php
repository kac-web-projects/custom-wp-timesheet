<?php
/**
 * @package Timesheet
 * @version 1
 */
/*
Plugin Name: Timesheet (Independent)
Plugin URI: http://wordpress.org/plugins/timesheet/
Description: A complete timesheet management tool
Author: Ketan Chawda
Version: 1
Author URI: http://www.chawda-ketan.com/
*/
defined('ABSPATH') or die("Sorry, you can't access this url directly - Security established");

// some useful definitions
define('TS_VERSION', 1.5);
define('TS_PLUGIN_DIR', dirname(__FILE__));
define('TS_PLUGIN_URL', plugins_url( '', __FILE__ ));
define('PLUGIN_URL', plugins_url( 'timesheet'));

//
// include_once TS_PLUGIN_DIR.'/widget.query.php';
// include_once TS_PLUGIN_DIR.'/admin/admin-page.inc';


// Necessary functions to show a query
// include_once TS_PLUGIN_DIR.'/admin/theme.inc';

//Schema for install & uninstall
// include_once TS_PLUGIN_DIR.'/schema.inc';
include_once TS_PLUGIN_DIR.'/on-activate.inc';
include_once TS_PLUGIN_DIR.'/on-deactivate.inc';
include_once TS_PLUGIN_DIR.'/email/mail.inc';


//Pages
include_once TS_PLUGIN_DIR.'/pages/page-view-dockets.inc';
include_once TS_PLUGIN_DIR.'/pages/page-view-dockets-bystatus.inc';
include_once TS_PLUGIN_DIR.'/pages/page-search-docket.inc';

include_once TS_PLUGIN_DIR.'/pages/page-create-docket.inc';
include_once TS_PLUGIN_DIR.'/pages/page-edit-docket.inc';
include_once TS_PLUGIN_DIR.'/pages/page-delete-docket.inc';

include_once TS_PLUGIN_DIR.'/pages/page-view-timesheets.inc';
// include_once TS_PLUGIN_DIR.'/pages/page-view-timesheets-bystatus.inc';
include_once TS_PLUGIN_DIR.'/pages/page-add-timesheet.inc';
include_once TS_PLUGIN_DIR.'/pages/page-edit-timesheet.inc';
include_once TS_PLUGIN_DIR.'/pages/page-delete-timesheet.inc';

include_once TS_PLUGIN_DIR.'/pages/page-view-templates.inc';
include_once TS_PLUGIN_DIR.'/pages/page-edit-template.inc';

// include_once TS_PLUGIN_DIR.'/pages/page-view-timesheets.inc';

//Handlers
include_once TS_PLUGIN_DIR.'/handlers/handler-docket-create.inc';
include_once TS_PLUGIN_DIR.'/handlers/handler-docket-update.inc';
include_once TS_PLUGIN_DIR.'/handlers/handler-docket-delete.inc';
include_once TS_PLUGIN_DIR.'/handlers/handler-docket-search.inc';
include_once TS_PLUGIN_DIR.'/handlers/handler-load-timesheets.inc';


include_once TS_PLUGIN_DIR.'/handlers/handler-timesheet-add.inc';
include_once TS_PLUGIN_DIR.'/handlers/handler-timesheet-update.inc';
include_once TS_PLUGIN_DIR.'/handlers/handler-timesheet-delete.inc';

include_once TS_PLUGIN_DIR.'/handlers/handler-template-update.inc';


add_action( 'admin_menu', 'ts_menu', 9999);

/*
 * Implementing my => hook_menu -> ts_menu
 */
function ts_menu()
{
  global $menu;
  // get the first available menu placement around 30, trivial, I know
  $menu_placement = 100;
  for($i = 30; $i < 100; $i++){
    if(!isset($menu[$i])){ $menu_placement = $i; break; }
  }
  // http://codex.wordpress.org/Function_Reference/add_menu_page
  $list_page    = add_menu_page( 'Timesheet', 'Timesheet', 'test-1','timesheet','ts_view_dockets_page','', $menu_placement);
 //manage_options
 //timesheet_employee -> Can add,edit,delete own timesheet 
 
 //timesheet_manager -> Can add,edit,delete own timesheet 
  //+ Can view other's timesheet too 
  //+ Can create,edit,delete own docket
 
 //timesheet_super

  

  // http://codex.wordpress.org/Function_Reference/add_submenu_page
  //Pages Menu - URLs (Dockets)
  $view_docket    = add_submenu_page( 'timesheet', 'View','View dockets', 'manage_options','view-dockets','ts_view_dockets_page');
  $view_docket_bystatus    = add_submenu_page( 'timesheet', 'View dockets bystatus','Dockets bystatus', 'manage_options','view-dockets-bystatus','ts_view_dockets_bystatus_page');
  $search_docket    = add_submenu_page( 'timesheet', 'Search docket','Search docket', 'manage_options','search-docket','ts_search_docket_page');

  $create_docket    = add_submenu_page('timesheet', 'Create','Create docket', 'manage_options','create-docket','ts_create_docket_page');
  $edit_docket    = add_submenu_page('hiding', 'Edit','Edit docket', 'manage_options','edit-docket','ts_edit_docket_page');
  $delete_docket = add_submenu_page('hiding','Delete','Delete docket','manage_options','delete-docket','ts_delete_docket_page');

  //Handlers - URLs (Dockets)
  $create_handler = add_submenu_page('hiding','Create Handler','Docket Create Handler','manage_options','docket-create','ts_create_docket_handler');
  $update_handler =	add_submenu_page('hiding','Update Handler','Docket Update Handler','manage_options','docket-update','ts_update_docket_handler');
  $delete_handler =	add_submenu_page('hiding','Delete Handler','Docket Update Handler','manage_options','docket-delete','ts_delete_docket_handler');
  $search_handler = add_submenu_page('hiding','Search Handler','Docket Search Handler','manage_options','docket-search','ts_search_docket_handler');
  $load_ts_handler = add_submenu_page('hiding','Load Timesheets Handler','Docket Load Timesheet Handler','manage_options','load-timesheets','ts_load_timesheets_handler');
  // $load_ts_handler = add_submenu_page('hiding','Load Timesheets Handler','Docket Load Timesheet Handler','manage_options','load-timesheets','ts_load_timesheets_handler');

  //Page Menu - URLs (Timesheets )
  $view_timesheet    = add_submenu_page('timesheet', 'View','View timesheet', 'manage_options','view-timesheets','ts_view_timesheets_page');
  // $view_timesheet_bystatus    = add_submenu_page('timesheet', 'View Timesheet bystatus','Timesheet bystatus', 'manage_options','view-timesheets-bystatus','ts_view_timesheets_bystatus_page');
  $add_timesheet    = add_submenu_page('timesheet', 'Add','Add timesheet', 'manage_options','add-timesheet','ts_add_timesheet_page');
  $edit_timesheet    = add_submenu_page('hiding', 'Edit','Edit timesheet', 'manage_options','edit-timesheet','ts_edit_timesheet_page');
  $delete_timesheet    = add_submenu_page('hiding', 'Delete','Delete timesheet', 'manage_options','delete-timesheet','ts_delete_timesheet_page');

  //Handlers - URLs (Dockets)
  $add_handler =	add_submenu_page('hiding','Add Handler','Timesheet Add Handler','manage_options','timesheet-add','ts_add_timesheet_handler');
  $tupdate_handler =	add_submenu_page('hiding','Update TimeS Handler','Timesheet Update Handler','manage_options','timesheet-update','ts_update_timesheet_handler');
  $tdelete_handler =	add_submenu_page('hiding','Delete TimeS Handler','Timesheet Delete Handler','manage_options','timesheet-delete','ts_delete_timesheet_handler');

  //Page Menu - URLs (Extra settings )
  $view_templates    = add_submenu_page('timesheet', 'View','View templates', 'manage_options','view-templates','ts_view_templates_page');
  $edit_template    = add_submenu_page('hiding', 'Edit','Edit template', 'manage_options','edit-template','ts_edit_template_page');

  $tpupdate_handler =  add_submenu_page('hiding','Update Template Handler','Template Update Handler','manage_options','template-update','ts_update_template_handler');

}

//Registering the activation - hook
register_activation_hook(__FILE__,'timesheet_activation'); //Custom function in schema.inc file
register_activation_hook(__FILE__,'timesheet_activation'); //Custom function in schema.inc file

//Registering the de-activation - hook
register_deactivation_hook(__FILE__,'timesheet_deactivation');//Custom function in schema.inc file
// register_deactivation_hook(__FILE__,'remove_plugin_caps');//Custom function in schema.inc file


add_action( 'admin_init', 'add_plugin_caps');
// add_action( 'admin_init', 'remove_plugin_caps');



