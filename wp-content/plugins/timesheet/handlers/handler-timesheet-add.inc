<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';
include_once TS_PLUGIN_DIR .'/queries.inc';
include_once TS_PLUGIN_DIR.'/email/mail.inc';


function ts_add_timesheet_handler() {

	$rdate = $_POST['ts-rdate'];
	$task_num = $_POST['ts-task-num'];
	$doc_num = $_POST['ts-doc-num'];
	$time_spent = $_POST['ts-time-spent'];
	$task_desc = $_POST['ts-task-desc'];
	
	$user = wp_get_current_user();
	$added_by = $user->user_login;

	//If extra space is been added at start or end
	$task_num = trim($task_num);
	$doc_num = trim($doc_num);
	$time_spent = trim($time_spent);
	$task_desc = trim($task_desc);

	global $wpdb;
	$table_name = $wpdb->prefix . "timesheet_master"; 
	$record_exist = check_duplicate($rdate,$task_num,$doc_num);
	if(empty($record_exist)) {
		$result = $wpdb->query(
			$wpdb->prepare( 
			"
			INSERT INTO $table_name
			(rdate, task_num, docket_num, time_spent, task_desc, added_by)
			VALUES ( %s, %s, %s, %s, %s, %s)
			", 
			array(
				$rdate,
				$task_num,
				$doc_num,
				$time_spent,
				$task_desc,
				$added_by
			) 
		));

		$wpdb->show_errors();

		if($result) {
			
			brh1color('New timesheet entry added successfully!','green');
			brh2color('Below are entry details : ','darkkhaki');

			brstrong_normal('Record date', $rdate);
			brstrong_normal('Task number', $task_num);
			brstrong_normal('Docket number' , $docket_num);
			brstrong_normal('Time spent', $time_spent);
			brstrong_normal('Task desc', $task_desc);
			brstrong_normal('Added by', $added_by);

			check_budget($doc_num);
		}
		else {
			brh1color('Something went wrong!','red');
			brh4color("Sorry for inconvience, please <a href=#> Try again</a>",'darkkhaki');
		}
	}
	else {

		brh3color("Record already exists. Record-date, task number & docket number must be unique always!",'red');
		brh1color('Below are record details :','darkkhaki');
		brstrong_normal('Docket number',$record_exist[0]->docket_num);
		$rid = $record_exist[0]->rid;

		brstrong_normal('Record date', $record_exist[0]->rdate);
		brstrong_normal('Task number', $record_exist[0]->task_num);
		brstrong_normal('Docket number', $record_exist[0]->doc_num);
		brstrong_normal('Time spent', $record_exist[0]->time_spent);
		brstrong_normal('Task desc', $record_exist[0]->task_desc);
		brstrong_normal('Added by', $record_exist[0]->added_by);

		brh4color("You can edit it <a href=\"?page=edit-timesheet&rid=$rid\" target=\"_blank\" >here</a>",'green');
	}

	ob_start();?>
		
		<div class="ts-update">
      	<br/><br/>
      	<input type="button" value="View timesheets" onClick='location.href="?page=view-timesheets"' class="button-primary" />
      	<input type="button" value="Create new entry" onClick='location.href="?page=add-timesheet"' class="button-primary" />
      	
    	</div>
    	
    	<?php
		echo ob_get_clean();
}