<?php
include_once TS_PLUGIN_DIR . '/html_format.inc';

function ts_delete_docket_page() {
	$href = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

	$url = parse_url($href);
	
	$query = array();
	parse_str($url['query'], $query);

	$did = $query['did'];
	$load_info = get_docket_info_did($did);

ob_start();?>

	<div id="ts-edit-docket">
    	<?php brh1color('Delete docket?','red'); ?>

  <form action='admin.php?page=docket-delete' method='post'>

	<!-- Field for docket-id -->
      <input class="ts-edit-input" type="hidden" name="ts-did" value="<?php print $did; ?>" readonly="readonly" />
    <?php

	    brstrong_normal('Docket number', $load_info[0]->docket_num);
		brstrong_normal('Description', $load_info[0]->docket_desc);
		brstrong_normal('Max budget' , $load_info[0]->docket_budget);
		brstrong_normal('Start date', $load_info[0]->start_date);
		brstrong_normal('End date', $load_info[0]->end_date);
		brstrong_normal('Notify', $load_info[0]->email_onmax);
		brstrong_normal('Notify email', $load_info[0]->notify_email);
		brstrong_normal('Added by', $load_info[0]->added_by);
	?>

    <!-- Confirm and Cancel button -->
    <div class="ts-create">
      <br/><br/>
      <input type="submit" value="Confirm" class="button-primary" />
      <input type="button" value="Cancel" onClick="history.go(-1)" class="button-primary" />
    </div>
   
  </form>
</div>

<?php
echo ob_get_clean();
}