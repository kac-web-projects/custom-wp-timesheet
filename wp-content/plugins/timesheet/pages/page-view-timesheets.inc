<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';
include_once TS_PLUGIN_DIR .'/queries.inc';


function ts_view_timesheets_page() {
ob_start();?>

	<div id="ts-view-timesheets">
    	<?php
      brh1color('View Timesheets','green');
      ?>

    <table style="width:300px">

      <thead>
      <tr>
      
      <?php
        th_width('#','20px');
        th_width('Entry-date','80px');
        th_width('Task-num','100px');
        th_width('Docket','100px');
        th_width('Time-spent (hrs)','30px');
        th_width('Task-desc','200px');
        th_width('Added-by','60px');
        th_width('EDIT','40px');
        th_width('DEL','40px');
      ?>

      </tr>
      </thead>

      <tbody>
      <?php
      $records = get_timesheets();
      $srno = 0;
      $empty = TRUE;
      foreach ($records as $record) {
        $empty = FALSE;
        print("<tr>");
        
        td_width(++$srno,'20px');
        td_width($record->rdate,'80px');
        td_width($record->task_num,'100px');
        td_width($record->docket_num,'100px');
        td_width($record->time_spent,'30px');
        td_width($record->task_desc,'200px');
        td_width($record->added_by,'60px');

        td_width("<a href=\"?page=edit-timesheet&rid=$record->rid\" target=\"_blank\"> Edit</a>",'40px');
        td_width("<a href=\"?page=delete-timesheet&rid=$record->rid\" target=\"_blank\"> Delete</a>",'40px');

        print("</tr><tr><td colspan=30><hr/></td></tr>");
      }
      if($empty) {
          print("<tr rowspan=4><td colspan=10>");
          brh4color("<br/>No timesheet entry found!<br/><br/>",'darkkhaki');
          print("</td></tr>");
      }

      ?>

      <tr rowspan = "4">
      <td colspan = "10">
      <input type="button" value="Add timesheet" onClick='location.href="?page=add-timesheet"' class="button-primary" />
      </td>
      </tr>

      </tbody>
    </table>
  	
</div>

<?php
echo ob_get_clean();
}