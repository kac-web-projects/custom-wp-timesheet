<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';
include_once TS_PLUGIN_DIR .'/queries.inc';

function ts_load_timesheets_handler() {

	$doc_num = $_POST['ts-doc-num'];
	$records = get_timesheet_info_dnum($doc_num);
	
	if($records) {
		
		brh1color("Timesheet(s) loaded for Docket($doc_num) successfully!",'green');
		brh2color('Below are timesheet(s) details','darkkhaki');
		ob_start();?>

	<div id="ts-load-timesheets">
    
    <table style="width:300px">

      <thead>
      <tr>
      
      <?php
        th_width('#','20px');
        th_width('Entry-date','80px');
        th_width('Task-num','100px');
        th_width('Time-spent','30px');
        th_width('Task-desc','200px');
        th_width('Added-by','60px');
        th_width('EDIT','40px');
        th_width('DEL','40px');
      ?>

      </tr>
      </thead>

      <tbody>
      <?php
      $srno = 0;
      $empty = TRUE;
      foreach ($records as $record) {
        $empty = FALSE;
        print("<tr>");
        
        td_width(++$srno,'20px');
        td_width($record->rdate,'80px');
        td_width($record->task_num,'100px');
        td_width($record->time_spent,'30px');
        td_width($record->task_desc,'200px');
        td_width($record->added_by,'60px');

        td_width("<a href=\"?page=edit-timesheet&rid=$record->rid\" target=\"_blank\"> Edit</a>",'40px');
        td_width("<a href=\"?page=delete-timesheet&rid=$record->rid\" target=\"_blank\"> Delete</a>",'40px');

        print("</tr><tr><td colspan=30><hr/></td></tr>");
      }
      if($empty) {
          print("<tr rowspan=4><td colspan=10>");
          brh4color("<br/>No timesheet entry found!<br/><br/>",'darkkhaki');
          print("</td></tr>");
      }

      ?>
	<?php
	echo ob_get_clean();
	
	}
	else {
		brh1color('Something went wrong!','red');
		brh4color("Sorry for inconvience, please <a href=#> Try again</a>",'darkkhaki');
	}
    
	ob_start();?>

		     </tbody>
    </table>

		<div class="ts-update">
      	<br/><br/>
      	<input type="button" value="Goto search (back)" onClick="history.go(-1)" class="button-primary" />
      	<input type="button" value="View dockets" onClick='location.href="?page=view-dockets"' class="button-primary" />
      	<input type="button" value="By status" onClick='location.href="?page=view-dockets-bystatus"' class="button-primary" />
      	
    	</div>
    	
    	<?php
		echo ob_get_clean();
}