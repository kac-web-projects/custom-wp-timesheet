<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';
include_once TS_PLUGIN_DIR .'/queries.inc';

function ts_delete_docket_handler() {

	$did = $_POST['ts-did'];
	// print($did);
	global $wpdb;
	$table_name = $wpdb->prefix . "timesheet_dockets"; 

	$result = $wpdb->delete($table_name, array('did' => $did));

	if($result) {
		brh1color('Docket deleted successfully!','green');
	}
	else {
		brh1color('Something went wrong!','red');
		brh4color("Sorry for inconvience, please <a href=#> Try again</a>",'green');
	}

	ob_start();?>
		
		<div class="ts-update">
      	<br/><br/>
      	<input type="button" value="View dockets" onClick='location.href="?page=view-dockets"' class="button-primary" />
      	<input type="button" value="By status" onClick='location.href="?page=view-dockets-bystatus"' class="button-primary" />
    	</div>
    	
    	<?php
		echo ob_get_clean();


}