<?php

function check_budget($doc_num){
	// error_log("checking hours");
	$hr_spent = count_timesheet_hours($doc_num);
	error_log($hr_spent);
	$result = get_docket_info_dnum($doc_num);
	$budget = $result[0]->docket_budget;
	error_log($budget);


	if($budget <= $hr_spent) {
		error_log('sending mail since budget reached');
		send_mail($result,$hr_spent);
	}
}

//
function send_mail($record, $hr_spent) {

	$email_onmax = $record[0]->email_onmax;
	$notify_email = $record[0]->notify_email;

	if($email_onmax == 'yes') {
		// Example using the array form of $headers

		$headers[] = 'From: Timesheet Management  <jobs@chawda-ketan.com>';
		// $headers[] = 'Cc: Ketan Aol <ketan_chawda@aol.com>';
		// $headers[] = 'Cc: ketan.portfolio@gmail.com'; // note you can just use a simple email address

		$templates = get_template_info_tid('1');
		$body = $templates[0]->body;
		$subject = $templates[0]->subject;

		$fields = filter_token_scan($body);

		foreach($fields as $field) {
		$pure_field = str_replace('{', '', $field);
		$pure_field = str_replace('}', '', $pure_field);
		
		$replace = $record[0]->$pure_field;	
		// print("<br/> Purefield : $pure_field");
		// print("<br/> Replace : $replace");

		$body = str_replace($field, $replace,$body);	
		}

		brh3color('An email has been sent to manager since docket reached the max budget','darkkhaki');

		add_filter('wp_mail_content_type','set_html_type');
		wp_mail($notify_email, $subject, $body, $headers );
		remove_filter('wp_mail_content_type','set_html_type');

	}

}
//Mail content type
function set_html_type() {
	return 'text/html';
}

function filter_token_scan($body) {
  // Matches tokens with the following pattern: {$fieldname}
  // $type may not contain : or whitespace characters, but $name may.
  preg_match_all('/
    \{             # { - pattern start
    ([^\s\[\]:]*)  # match $type not containing whitespace : [ or ]
    \}             # } - pattern end
    /x', $body, $matches);

  $fields = $matches[1];

  // Iterate through the matches, building an associative array containing
  $results = array();
  for ($i = 0; $i < count($fields); $i++) {
  	array_push($results, $matches[0][$i]);
  }

  return $results;
}