<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';
include_once TS_PLUGIN_DIR .'/queries.inc';


function ts_view_timesheets_bystatus_page() {
ob_start();?>
<head>
  <script src="<?php print(PLUGIN_URL . '/js/pages.js'); ?>"></script>
</head>

	<div id="ts-view-timesheets">
    	<?php
      brh1color('View Timesheets','green');
      ?>
    <input type="button" value="Active dockets" class="button-primary" onclick="javascript:docket_display('active','inactive','hold');"/>
  
    <input type="button" value="Inactive dockets" class="button-primary" onclick="javascript:docket_display('inactive','active','hold');"/>
  
    <input type="button" value="Hold dockets" class="button-primary" onclick="javascript:docket_display('hold','active','inactive');"/>
    <br/><br/><br/>

      <?php
      $status = array('active','inactive','hold');

      for($i = 0; $i < 3; $i++) {
        if($i == 0)
        print("<div id=\"$status[$i]\" >");//Start div
        else
        print("<div id=\"$status[$i]\" style=\"display:none\">");//Start div

        ob_start();?>
    

    <table style="width:300px">

      <thead>
      <tr>
        <th>#</th>
        <th>Entry</th>
        <th>_</th>
        <th>date</th>

        <th>Task</th>
        <th>_</th>
        <th>num</th> 
        
        <th>Docket</th> 

        <th>Time</th>
        <th>_</th>
        <th>spent</th>
        
        <th>Task</th>
        <th>_</th>
        <th>desc</th>
        <th> </th>
        <th> </th>


        <th>Added by</th>
        
        <th> </th>
        <th>EDIT</th>

        <th>DEL</th>

      </tr>
      </thead>

      <tbody>
      <?php
        echo ob_get_clean();
              $records = get_timesheets();
              $srno = 0;
              $empty = TRUE;
              foreach ($records as $record) {
                $empty = FALSE;
                print("<tr><td>" . ++$srno . '</td>');
                print("<td colspan=\"3\">" . $record->rdate . '</td>');
                print("<td colspan=\"3\">" . $record->task_num . '</td>');
                print("<td>" . $record->docket_num . '</td>');
                print("<td colspan=\"3\">" . $record->time_spent . '</td>');
                print("<td colspan=\"5\">" . $record->task_desc . '</td>');
                print("<td>" . $record->added_by . '</td>');

                print("<td><a href=\"?page=edit-timesheet&rid=$record->rid\" target=\"_blank\"> Edit</a></td>");

                print("<td><a href=\"?page=delete-timesheet&rid=$record->rid\" target=\"_blank\"> Delete</a></td>");

                print("</tr><tr><td colspan=30><hr/></td></tr>");
              }

              if($empty) {
                  print("<tr rowspan=4><td colspan=10><br/><br/>No timesheet entry found!<br/><br/></td></tr>");
              }
              ob_start();?>

              <tr rowspan = "4">
              <td colspan = "10">
              <input type="button" value="Add timesheet" onClick='location.href="?page=add-timesheet"' class="button-primary" />
              </td>
              </tr>

              </tbody>
              </table>
              <?php
              echo ob_get_clean();
        
        print("</div>");//Close div
        }
    ?>

</div>

<?php
echo ob_get_clean();
}