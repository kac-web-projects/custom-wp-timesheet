<?php

//br strong
function brstrong($text) {
	print("<br/><strong>$text</strong>");
}

//br strong and normal text
function brstrong_normal($first_arg,$second_arg) {
	print("<br/><strong>$first_arg : </strong> $second_arg");
}

//br + h1
function brh1($text) {
	print("<br/><h1>$text</h1>");
}

//br + h2
function brh2($text) {
	print("<br/><h2>$text</h2>");
}

//br + h3
function brh3($text) {
	print("<br/><h3>$text</h3>");
}

//br + h4
function brh4($text) {
	print("<br/><h4>$text</h4>");
}

//br + css color
function brcolor($text, $color) {
	print("<br/><span style=color:$color> $text </span>");
}

//br h1 + css color
function brh1color($text, $color) {
	print("<br/><h1><span style=color:$color> $text </span></h1>");
}

//br h2 + css color
function brh2color($text, $color) {
	print("<br/><h2><span style=color:$color> $text </span></h2>");
}

//br h3 + css color
function brh3color($text, $color) {
	print("<br/><h3><span style=color:$color> $text </span></h3>");
}

//br h4 + css color
function brh4color($text, $color) {
	print("<br/><h4><span style=color:$color> $text </span></h4>");
}

//th width block h4 + css color
function th_width($text,$width) {
	print("<th style=\"min-width:$width;\"> $text </th>");
}

//td width block h4 + css color
function td_width($text,$width) {
	print("<td style=\"min-width:$width;\"> $text </td>");
}
