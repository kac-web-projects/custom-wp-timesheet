<?php

function ts_delete_timesheet_page() {
	$href = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

	$url = parse_url($href);
	
	$query = array();
	parse_str($url['query'], $query);

	$rid = $query['rid'];
	print($did);
	$load_info = get_timesheet_info_rid($rid);

ob_start();?>

	<div id="ts-edit-timesheet">
    	<?php brh1color('Delete this timesheet entry?','red'); ?>

  <form action='admin.php?page=timesheet-delete' method='post'>

	<!-- Field for r-id -->
      <input class="ts-edit-input" type="hidden" name="ts-rid" value="<?php print $rid; ?>" readonly="readonly" />
      <?php

	    brstrong_normal('Record date', $load_info[0]->rdate);
		brstrong_normal('Task number', $load_info[0]->task_num);
		brstrong_normal('Docket number', $load_info[0]->doc_num);
		brstrong_normal('Time spent', $load_info[0]->time_spent);
		brstrong_normal('Task desc', $load_info[0]->task_desc);
		brstrong_normal('Added by', $load_info[0]->added_by);
	  ?>
    <!-- Confirm and Cancel button -->
    <div class="ts-create">
      <br/><br/>
      <input type="submit" value="Confirm" class="button-primary" />
      <input type="button" value="Cancel" onClick="history.go(-1)" class="button-primary" />
    </div>
   
  </form>
</div>

<?php
echo ob_get_clean();
}