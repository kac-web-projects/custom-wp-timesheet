<?php
include_once TS_PLUGIN_DIR . '/html_format.inc';
include_once TS_PLUGIN_DIR . '/queries.inc';

function ts_add_timesheet_page() {
ob_start();?>
  <head>
    <script src="<?php print(PLUGIN_URL . '/jquery/jquery-2.1.1.min.js'); ?>"></script>

    <script src="<?php print(PLUGIN_URL . '/jquery/select2-master/select2.js'); ?>"></script>

    <link href="<?php print(PLUGIN_URL . '/jquery/select2-master/select2.css'); ?>" rel="stylesheet"/>
    <script>
        $(document).ready(function() {
          $("#ts-doc-num").select2();
        });
    </script>
  </head>

  <div id="ts-timesheet">
    <?php brh1color('Add timesheet','green'); ?>

  <form action='admin.php?page=timesheet-add' method='post'>
  	
    <!-- Field for Record-date -->
    <div class="ts-setting">
      <label class="ts-label">Record date :</label>
      <input class="ts-create-input" type="date" name="ts-rdate" value="" required="true" />
      <p class="description">timesheet entry for date</p>
    </div>

    <!-- Field for task-number -->
    <div class="ts-setting">
      <label class="ts-label">Task number :</label>
      <input class="ts-create-input" type="text" name="ts-task-num" value="" required="true" size=4 maxlength="3" />
      <p class="description">Task number</p>
    </div>

    <!-- Field for docket-number -->
    <div class="ts-setting">
      <label class="ts-label">Docket number :</label>
      <select class="ts-create-input" id="ts-doc-num" name="ts-doc-num"  style="width:200px" required="true" />
        <?php 
        $dockets = get_dockets_bystatus('active');
        foreach ($dockets as $docket) {
          print("<option value=\"$docket->docket_num\">$docket->docket_num</option>");  
        }
        ?>
      </select>
      <p class="description">Docket number, start typing</p>
    </div>

    <!-- Field for task-spent -->
    <div class="ts-setting">
      <label class="ts-label">Time spent : </label>
      <input class="ts-create-input" name="ts-time-spent" required="true" size=3 maxlength="4" > /hr
      <p class="description">enter the time spent on task, such as 0.25 for 15 mins</p>
    </div>

  	<!-- Field for task-description -->
    <div class="ts-setting">
      <label class="ts-label">Task description :</label>
      <br/>
      <textarea class="ts-create-input" name="ts-task-desc" rows="4" cols="70" required="true" >some description about task 
       </textarea> 
      <p class="description">enter the task description</p>
    </div>

    <!-- submit and reset button -->
    <div class="ts-add">
      <br/><br/>
      <input type="submit" value="Add" class="button-primary" />
      <input type="button" value="Reset" onClick="history.go(0)" class="button-primary" />
      <input type="button" value="View timesheets" class="button-primary" onClick='location.href="?page=view-timesheets"' />
    </div>

  </form>
</div>

<?php
echo ob_get_clean();
}