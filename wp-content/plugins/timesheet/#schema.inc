<?php
/*
Custom function to invoke when plugin is enabled (activated)
*/
function timesheet_activation(){
	global $wpdb;

	/*
	 * We'll set the default character set and collation for this table.
	 * If we don't do this, some characters could end up being converted 
	 * to just ?'s when saved in our table.
	 */
	$charset_collate = '';

	if ( ! empty( $wpdb->charset ) ) {
	  $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
	}

	if ( ! empty( $wpdb->collate ) ) {
	  $charset_collate .= " COLLATE {$wpdb->collate}";
	}

   	$table_name = $wpdb->prefix . "timesheet_dockets"; 
	create_docket_table($table_name,$charset_collate); //custom function to install docket table
   	
   	$table_name = $wpdb->prefix . "timesheet_master"; 
	create_timesheet_table($table_name,$charset_collate); //custom function to install timesheet table

	error_log("Notify : Timesheet plugin activated");
}

/*
Custom function to invoke when plugin is disable(deactivated)
*/
function timesheet_deactivation(){
	error_log("Notify : Timesheet plugin de-activated");
	drop_table("timesheet_dockets");
   	drop_table("timesheet_master");
}

/*
Creating timesheet_dockets table on Plugin Activated.
*/
function create_docket_table($table_name, $charset_collate) {
	
	$sql = "CREATE TABLE $table_name (
	  did mediumint(9) NOT NULL AUTO_INCREMENT,
	  docket_num VARCHAR(30) NOT NULL,
	  docket_desc VARCHAR(255) DEFAULT '' NOT NULL,
	  docket_budget tinytext NOT NULL,
	  start_date date DEFAULT '0000-00-00' NOT NULL,
	  end_date date DEFAULT '0000-00-00' NOT NULL,
	  email_onmax text DEFAULT 'no' NOT NULL,
	  notify_email VARCHAR(50) default 'user@example.com' NOT NULL,
	  docket_status VARCHAR(10) default 'active' NOT NULL,

	  added_by VARCHAR(20) DEFAULT '' NOT NULL,

	  PRIMARY KEY (did),
	  UNIQUE KEY (docket_num)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta($sql);
	error_log("Notify : timesheet_dockets table created");
}

/*
Creating timesheet_master table on Plugin Activated.
*/
function create_timesheet_table($table_name, $charset_collate) {
	 
	$sql = "CREATE TABLE $table_name (
	  rid mediumint(9) NOT NULL AUTO_INCREMENT,
	  rdate date DEFAULT '0000-00-00' NOT NULL,
	  task_num tinytext NOT NULL,
	  docket_num VARCHAR(30) NOT NULL,
	  time_spent text NOT NULL,
	  task_desc VARCHAR(255) DEFAULT '' NOT NULL,
	  status text NOT NULL,
	  
	  added_by VARCHAR(20) DEFAULT '' NOT NULL,
	  
	  PRIMARY KEY (rid)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta($sql);
	error_log("Notify : timesheet_master table created");
}

/*
Removing Installing timesheet_dockets table
*/
function drop_table($table_name) {
	global $wpdb;
	$table_name = $wpdb->prefix . $table_name;
	$sql = "DROP TABLE IF EXISTS $table_name;";
	$e = $wpdb->query($sql);
	// die(var_dump($e));

	error_log("Notify : $table_name table is been removed");
}

//Granting Editor role permission to manege_options
function add_plugin_caps() {
    // gets the author role
    $role = get_role( 'editor' );

    // This only works, because it accesses the class instance.
    // would allow the author to edit others' posts for current theme only
    $role->add_cap( 'manage_options' ); 
}

//Revoke Editor role permission to manege_options
function remove_plugin_caps() {
    // gets the author role
    $role = get_role( 'editor' );

    // This only works, because it accesses the class instance.
    // would allow the author to edit others' posts for current theme only
    $role->remove_cap( 'manage_options' ); 
}
