<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';

function ts_search_docket_page() {

ob_start();?>
  <head>
    <script src="<?php print(PLUGIN_URL . '/jquery/jquery-2.1.1.min.js'); ?>"></script>

    <script src="<?php print(PLUGIN_URL . '/jquery/select2-master/select2.js'); ?>"></script>

    <link href="<?php print(PLUGIN_URL . '/jquery/select2-master/select2.css'); ?>" rel="stylesheet"/>
    <script>
        $(document).ready(function() {
          $("#ts-doc-num").select2();
        });
    </script>
  </head>



  <div id="ts-search-dockets">
  <form action='admin.php?page=docket-search' method='post'>
      <?php
      brh1color('Search dockets','green');
      ?>

    <!-- Field for docket-number -->
    <div class="ts-setting">
      <label class="ts-label">Docket number :</label>
      <select class="ts-create-input" id="ts-doc-num" name="ts-doc-num"  style="width:200px" required="true" />
        <?php 
        $dockets = get_dockets();
        foreach ($dockets as $docket) {
          print("<option value=\"$docket->docket_num\">$docket->docket_num</option>");  
        }
        ?>
      </select>
    <input type="submit" value="Load docket" class="button-primary" />
  
    <input type="submit" value="Load timesheet" formaction="?page=load-timesheets" class="button-primary" />
    <p class="description">Docket number, start typing</p>
    </div>
    <input type="button" value="Reset" onClick='history.go(-1);' class="button-primary" />  
    </form>
  </div>

<?php
echo ob_get_clean();
}