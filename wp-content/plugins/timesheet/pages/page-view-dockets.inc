<?php
include_once TS_PLUGIN_DIR .'/html_format.inc';

function ts_view_dockets_page() {
ob_start();?>

	<div id="ts-view-dockets">
    	<?php
      brh1color('View dockets','green');
      ?>

    <table style="width:300px">

      <thead>
        <?php
          
            th_width('#','20px');
            th_width('Docket','100px');
            th_width('Description','200px');
            th_width('Budget (hrs)','40px');
            th_width('Start-date','80px');
            th_width('End-date','80px');
            th_width('Notify','40px');
            th_width('Email','150px');
            th_width('Status','50px');
            th_width('Added-by','60px');
            
            th_width('EDIT','40px');
            th_width('DEL','40px');

          ?>
    
      </thead>

      <tbody>
      <?php
      $records = get_dockets();
      $srno = 0;
      $empty = TRUE;
      foreach ($records as $record) {
        $empty = FALSE;

        print("<tr>");
                td_width(++$srno,'20px');
                td_width($record->docket_num,'100px');
                td_width($record->docket_desc,'200px');
                td_width($record->docket_budget,'40px');
                td_width($record->start_date,'80px');
                td_width($record->end_date,'80px');
                td_width($record->email_onmax,'40px');
                td_width($record->notify_email,'150px');
                td_width($record->docket_status,'50px');
                td_width($record->added_by,'60px');
                td_width("<a href=\"?page=edit-docket&did=$record->did\" target=\"_blank\"> Edit</a>",'40px');
                td_width("<a href=\"?page=delete-docket&did=$record->did\" target=\"_blank\"> Delete</a></td>",'40px');

                print("</tr><tr><td colspan=12><hr/></tr>");
              
      }

      if($empty) {
          print("<tr rowspan=4><td colspan=10>");
          brh4color("<br/>No docket entry found!<br/><br/>",'darkkhaki');
          print("</td></tr>");
      }
      ?>
   
      <tr rowspan = "4">
      <td colspan = "10">
      <input type="button" value="Add docket" onClick='location.href="?page=create-docket"' class="button-primary" />
      <input type="button" value="By status" onClick='location.href="?page=view-dockets-bystatus"' class="button-primary" />

      </td>
      </tr>

      </tbody>

    </table>
  	
</div>

<?php
echo ob_get_clean();
}